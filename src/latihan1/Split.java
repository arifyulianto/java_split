/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package latihan1;

import java.util.Arrays;

/**
 *
 * @author VOLD
 */
public class Split {
    public static void main(String[] args) {
         
      String dancers[] = {"Dancer 1 - Nathania, Yohanna, Bella", "Dancer 2 - Andrew, Michael, Jason", "Dancer 3 - Megan, Riana, Nicole"};
      
      for(int i=0; i < dancers.length; i++) {
        String[] splits = dancers[i].split("-");
        
        String number = splits[0];
        String dancer = splits[1]; 
        
        String[] splits_dancer = dancer.split(",");
        
        // Sort  
        Arrays.sort(splits_dancer);
        
        System.out.println(number + ":");
        for(int j=0; j<splits_dancer.length; j++) {
            System.out.println(splits_dancer[j]);
        }
      }
    }
}
