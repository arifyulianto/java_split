/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package latihan1;

import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author VOLD
 */
public class Latihan1 {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //declare variable        
        String employees[] = {"Kurnia Aji", "Pratama", "Prasetya", "Arif Yulianto"};
        
        // Sort  
        Arrays.sort(employees);
        
        System.out.println("A-Z");
        System.out.print("[");
        for (int x = 0; x < employees.length; x++) {
            if(employees[x] == employees[employees.length-1]) {
                System.out.print(employees[x]);
            } else {
                System.out.print(employees[x] + ", ");
            }
        }
        System.out.println("]");
        
        System.out.println("");
        
        System.out.println("Z-A");
        Arrays.sort(employees, Collections.reverseOrder());
        System.out.print("[");
        for (int x = 0; x < employees.length; x++) {
            if(employees[x] == employees[employees.length-1]) {
                System.out.print(employees[x]);
            } else {
                System.out.print(employees[x] + ", ");
            }
        }
        System.out.println("]");
        
        System.out.println("");
        
    }
    
}
